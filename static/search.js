var dataCard = [
    {
        "name":"MIT OpenCourseWare",
        "related":"MIT",
        "type":["Platform", "Online course", "Video"],
        "link":["Home page","https://ocw.mit.edu/index.htm"],
        "description":"MIT OpenCourseWare makes the materials used in the teaching of almost all of MIT's subjects available on the Web, free of charge. OCW is delivering on the promise of open sharing of knowledge.",
        "icon":"\uf0ac",
        "lang":"EN"
    },
    {
        "name":"Coursera",
        "related":"Coursera",
        "type":["Platform", "Online course", "Video"],
        "link":["Home page","https://www.coursera.org/"],
        "description":"Coursera provides universal access to the world’s best education, partnering with top universities and organizations to offer courses online. Free courses with paid certificates.",
        "icon":"\uf0ac",
        "lang":"EN"
    },
    {
        "name":"edX",
        "related":"edX",
        "type":["Platform", "Online course", "Video"],
        "link":["Home page","https://www.edx.org/"],
        "description":"edX is the trusted platform for education and learning. Founded by Harvard and MIT, edX is home to more than 20 million learners, the majority of top-ranked universities in the world and industry-leading companies. Free courses with paid certificates.",
        "icon":"\uf0ac",
        "lang":"EN"
    },
    {
        "name":"FutureLearn",
        "related":"FutureLearn",
        "type":["Platform", "Online course", "Video"],
        "link":["Home page","https://www.futurelearn.com/"],
        "description":"Learn 100% online with world-class universities and industry experts. Develop your career, learn a new skill, or pursue your hobbies with flexible online courses. Free courses with paid certificates.",
        "icon":"\uf0ac",
        "lang":"EN"
    },
    {
        "name":"Internet Archive",
        "related":"Internet Archive",
        "type":["Platform", "Book", "Video"],
        "link":["Home page","https://archive.org/"],
        "description":"The Internet Archive is building a digital library of Internet sites and other cultural artifacts in digital form. Like a paper library, we provide free access to the general public. Our mission is to provide Universal Access to All Knowledge.",
        "icon":"\uf0ac",
        "lang":"EN"
    },
    {
        "name":"The Feynman Lectures on Physics",
        "related":"Caltech",
        "type":["Book"],
        "link":["Home page","https://www.feynmanlectures.caltech.edu/"],
        "description":'The Feynman Lectures on Physics is a physics textbook based on some lectures by Richard Feynman, a Nobel laureate who has been called "The Great Explainer". The lectures were presented before undergraduate students at Caltech, during 1961–1963.',
        "icon":"\uf02d",
        "lang":"EN"
    },
    {
        "name":"Course of Theoretical Physics",
        "related":"Landau & Lifshitz",
        "type":["Video"],
        "link":["Home page","https://archive.org/search.php?query=%22A%20Course%20of%20Theoretical%20Physics%22%20AND%20landau"],
        "description":'The Course of Theoretical Physics is a ten-volume series of books covering theoretical physics that was initiated by Lev Landau and written in collaboration with his student Evgeny Lifshitz starting in the late 1930s.',
        "icon":"\uf02d",
        "lang":"EN"
    },
    {
        "name":"MIT OpenCourseWare Youtube",
        "related":"MIT",
        "type":["Video"],
        "link":["Channel","https://www.youtube.com/c/mitocw"],
        "description":"Whether you’re a student, a teacher, or simply a curious person that wants to learn, MIT OpenCourseWare (OCW) offers a wealth of insight and inspiration. There's videos, and a whole lot more!",
        "icon":"\uf03d",
        "lang":"EN"
    },
    {
        "name":"Center For Theoretical and Computational Physics Youtube",
        "related":"Center For Theoretical and Computational Physics",
        "type":["Video"],
        "link":["Channel","https://www.youtube.com/channel/UCYB6YI9MuGwyqdS54YjG1Rg?app=desktop"],
        "description":"CFTC is a research unit of the Faculty of Sciences of the University of Lisbon, with a broad range of projects in theoretical physics and its applications to other sciences.",
        "icon":"\uf03d",
        "lang":"PT"
    },
    {
        "name":"Khan Academy",
        "related":"Khan Academy",
        "type":["Platform", "Online course", "Video"],
        "link":["Home page","https://khanacademy.org/"],
        "description":"Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom.",
        "icon":"\uf0ac",
        "lang":"EN"
    },
]