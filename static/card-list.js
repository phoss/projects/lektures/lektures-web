$(function() {
  populateWithCards("#cardContainer", dataCard);
});

function populateWithCards( section, array ) {
  for (var i = 0; i < array.length; i++){
    var element = array[i];
    $(section).append(buildCard(element));
  }
}

function buildCard( element ) {
  return `
  <div class="card">
  <div class="card-back" icon-font="${element["icon"]}">
    <div class="code">
    <span class="boolean">Name: </span><span>${element["name"]}</span>
    <br/>
    <span class="boolean">Link: </span><a href="${element["link"][1]}">${element["link"][0]}</a>
    <br/>
    <span class="boolean">Description:</span>
    <br/>
    <span>${element["description"]}</span>
    </div>
  </div>
  <div class="card-front" icon-font="${element["icon"]}">
  <div class="code">
      <div class="method">${element["name"]}</div>
      <div><span class="boolean">${element["type"][0]}</span>, ${element["related"]}</div>
  </div>
</div>
`
}