---
title: "Associations"
description: "This page refers to the groups, organizations and entities associated with the project"
pre: <b><i class="fas fa-hands-helping"></i> </b>
weight: 5
---

# Associations

This page refers to the groups, organizations and entities associated with the project.

If you are part of an association that would like to contribute and be part of the project, feel free to contact us!