---
title: "Associações"
description: "Esta página refe-se aos grupos, organizações e entidades associadas com o projeto"
pre: <b><i class="fas fa-hands-helping"></i> </b>
weight: 5
---

# Associações

Esta página refe-se aos grupos, organizações e entidades associadas com o projeto.

Se faz parte de uma associação que gostaria de contribuir e fazer parte do projeto, sinta-se à vontade para entrar em contato connosco!