---
title: "Recursos"
description: "Esta página contém recursos (vídeos, cursos, livros, ...)"
pre: <b><i class="fas fa-book-reader"></i> </b>
weight: 3
---

# Recursos

Esta página contém recursos como vídeos, cursos online, livros e outros que possam ser utilizados de forma gratuita para estudo.

<script src="https://kit.fontawesome.com/5b6d7ec453.js"></script>
<link rel="stylesheet" href="../card.css">
<script src="../search.js"></script>
<script src="../card-list.js"></script>
<div id="cardContainer"></div>