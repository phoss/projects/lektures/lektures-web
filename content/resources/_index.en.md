---
title: "Resources"
description: "This page contains resources (videos, courses, books, ...)"
pre: <b><i class="fas fa-book-reader"></i> </b>
weight: 3
---

# Resources