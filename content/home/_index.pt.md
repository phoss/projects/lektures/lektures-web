---
title: "Home"
description: "Página inicial"
pre: <b><i class="fas fa-home"></i> </b>
weight: 1
---

# Home

Bem-vindo à página inicial do projeto lektures!

Este projeto é desenvolvido pelo grupo PhOSS.

Projeto
-------

O grupo PhOSS, com a criação do projeto lektures, permite aos seus futuros utilizadores - estudantes, curiosos, professores, etc - uma forma de estudo, nas áreas da Física e da Engenharia, diferente das dos meios convencionais, nomeadamente bancos de apontamentos ou repositórios das suas faculdades e/ou núcleos de estudantes, visto que esta plataforma permite que cada tópico seja abordado na sua verdadeira natureza e não apenas como elemento de um plano curricular.

Para além disso, usufrui de se encontrar sempre devidamente atualizado, pois cada utilizador poderá criar um ficheiro do seu interesse ou complementar um tópico com a informação que considere pertinente, cabendo depois aos administradores do grupo avaliar se é compatível ou não com as diretrizes do programa e assim, posteriormente, permitir a sua incorporação no banco de dados. Isto garante a qualidade do projeto e das adições propostas.

Em suma, este projeto, pensado e criado com base em necessidades experienciadas pelos seus criadores, vem tornar mais simples e acessível a procura de bons e eficazes elementos que complementem o estudo de todo e qualquer estudante universitário.

Conteúdos
---------

Este projeto, embora dê ênfase à física e à engenharia, está a desenvolver notas nas seguintes áreas:

-   Física
-   Engenharia
-   Matemática
-   Informática
-   Química

Tendo ainda guias de como usar software considerado pertinente para os temas abordados pelo projeto e como contribuir.

Ferramentas usadas
------------------

Este projeto está hospedado no GitLab, onde pode ser alterado por qualquer pessoa. Todas as contribuições directas são feitas através do GitLab com Git.

Esta plataforma é hospedada no GitLab, usando Git para manter as várias versões e colaboração entre membros, GoHugo para gerar o site e Pandoc para gerar documentos independentes do site que podem ser descarregados individualmente. Toda a automação corre com base na ferramentas CI/CD do GitLab num container Docker de ArchLinux.