---
title: "Home"
description: "Main page"
pre: <b><i class="fas fa-home"></i> </b>
weight: 1
---

# Home

Welcome to the lektures project home page!

This project is developed by the PhOSS group.

Project
-------

The PhOSS group, with the creation of the lektures project, allows its future users - students, curious people, teachers, etc - a way of study, in the fields of Physics and Engineering, different from those of conventional means, namely notebanks or repositories of their faculties and/or student associations, since this platform allows each topic to be approached in its true nature and not only as an element of a specific curricular plan.

In addition, it is always up to date, as each user can create a file of their interest or complement a topic with the information they consider relevant, and it is then up to a group of administrators to assess whether or not the changes are compatible with the program's guidelines and then allow its incorporation into the database. This guarantees the quality of the project and the proposed additions.

In short, this project, conceived and created based on the needs experienced by its creators, makes the search for good and effective elements that complement the study of each and every university student simpler and more accessible.

Contents
---------

This project, while emphasizing physics and engineering, is developing notes in the following areas:

- Physics
- Engineering
- Mathematics
- Informatics
- Chemistry

Having also guides on how to use software considered relevant to the fields addressed by the project and how to contribute.

Tools used
------------------

This project is hosted on GitLab where it can be changed by anyone. All direct contributions are made through GitLab with Git.

This platform is hosted on GitLab, using Git to maintain the various versions and collaboration between members, GoHugo to generate the site and Pandoc to generate documents independent of the site that can be downloaded individually. All automation runs based on GitLab's CI/CD tools in an ArchLinux Docker container.