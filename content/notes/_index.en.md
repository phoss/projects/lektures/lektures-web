---
title: "Notes"
description: "This is the page that contains all notes"
pre: <b><i class="fas fa-file-contract"></i> </b>
weight: 2
---

# Notes

This is the directory that contains all notes.