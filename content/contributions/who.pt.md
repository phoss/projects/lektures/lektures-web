---
title: Quem?
subtitle: Quem contribui para o projeto
comments: false
pre: <b><i class="fas fa-users"></i> </b>
---

Quem contribui?
===============

Aqui você pode verificar quem contribuiu para o projeto, se você é um indivíduo ou uma organização.

Associações
------------

<table>
    <thead>
        <tr>
            <th style='text-align:center;vertical-align:middle'>Associação</th>
            <th style='text-align:center;vertical-align:middle'>Contribuições</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 style='text-align:center;vertical-align:middle'><a href="https://gitlab.com/phoss">PhOSS</a></td>
            <td style='text-align:center;vertical-align:middle'>Plataforma</td>
        </tr>
        <tr>
            <td style='text-align:center;vertical-align:middle'>Conteúdo</td>
        </tr>
        <tr>
            <td style='text-align:center;vertical-align:middle'>Revisão</td>
        </tr>
        <tr>
            <td style='text-align:center;vertical-align:middle'>Tradução</td>
        </tr>
    </tbody>
</table>

Indivíduos
-----------