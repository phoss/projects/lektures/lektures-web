---
title: How?
subtitle: How to contribute to this project
comments: false
pre: <b><i class="fas fa-question-circle"></i> </b>
---

How to contribute?
==================

Here you can check how to contributed to the project.

All contributions
-----------------

Whatever kind of contributions you may want to do, there are two ways to do it: directly with Git or through the associations.

Direct contributions are submitted to our platform in GitLab and revised by our team before being incorporated. Although contributions *can* be made through GitLab's website, some knowledge of Git is recommended since it allows contributors to integrate submissions (or merge requests) to be made from their systems, allowing better integration with whatever development environment is being used.

Contributions can also be submitted to some of the associations that are part of the project. In this case the association is responsible to submit the contribution through Git. This process takes time and submittions may take a while to be reviewd. It is advisable to submit everything directly when possible.

Platform
--------

This platform runs due to a combination of several tools that are seamlessly integrated with each other.

### Website

If you know any of the web technologies used (HTML, CSS, JS, GoHugo) and would like to contribute, please refer to this [repository](https://gitlab.com/phoss/projects/lektures/lektures-web).

### Automation

If you know about automation (Bash, Docker, CI/CD) and would like to contribute, please refer to this [repository](https://gitlab.com/phoss/projects/lektures/lektures-deploy).
This is where the least amount of contributions are needed, so make sure you only submit something that greatly benefits the project (new features, more robust automation, etc.). Do not submit a proposal to change the Docker image for another distribution.

Content
--------

This is where contributions are most needed.
Any notes related to the relevant fields can be submitted either in markdown or reStructuredText format.

Revisions
---------

This is a key part of the project reserved to the associations involved and academically certified individuals.
Only members that took courses in the relevant field can accept content submissions of that field.

Translations
------------

Although the main focus of the project is Portuguese, the website can support other languages and does so.
For the time being, we accept translations or new content in Spannish, German and specially English.