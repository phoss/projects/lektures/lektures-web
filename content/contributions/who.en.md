---
title: Who?
subtitle: Who has contributed to this project
comments: false
pre: <b><i class="fas fa-users"></i> </b>
---

Who contributes?
================

Here you can check who contributed to the project, wheter you're an individual or an organization.

Associations
------------

<table>
    <thead>
        <tr>
            <th style='text-align:center;vertical-align:middle'>Associations</th>
            <th style='text-align:center;vertical-align:middle'>Contributions</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 style='text-align:center;vertical-align:middle'><a href="https://gitlab.com/phoss">PhOSS</a></td>
            <td style='text-align:center;vertical-align:middle'>Platform</td>
        </tr>
        <tr>
            <td style='text-align:center;vertical-align:middle'>Content</td>
        </tr>
        <tr>
            <td style='text-align:center;vertical-align:middle'>Revisions</td>
        </tr>
        <tr>
            <td style='text-align:center;vertical-align:middle'>Translation</td>
        </tr>
    </tbody>
</table>

Individuals
-----------