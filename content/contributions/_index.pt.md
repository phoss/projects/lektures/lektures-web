---
title: "Contribuições"
description: "Página das contribuições"
pre: <b><i class="fas fa-people-carry"></i> </b>
weight: 4
---

# Contribuições

Esta página refere as contribuições para o projeto.
Aqui pode encontrar-se informação sobre como contribuir e sobre quem já contribui.

Se você faz parte de uma organização que gostaria de fazer parte do projeto, sinta-se à vontade para entrar em contato connosco!