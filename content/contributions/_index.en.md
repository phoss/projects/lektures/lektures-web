---
title: "Contributions"
description: "Contributions page"
pre: <b><i class="fas fa-people-carry"></i> </b>
weight: 4
---

# Contributions

This page lists the contributions to the project.
Here you can find information on how to contribute and who is already contributing.

If you're part of an organization that would like to be a part of the project, feel free to contact us!
