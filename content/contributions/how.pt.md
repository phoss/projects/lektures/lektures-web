---
title: Como?
subtitle: Como contribuir para o projeto
comments: false
pre: <b><i class="fas fa-question-circle"></i> </b>
---

Como contribuir?
==================

Aqui pode descobrir como contríbuir para o projeto.

Todas as contribuições
----------------------

Qualquer que seja o tipo de contribuições que queira fazer, há duas formas de o fazer: directamente com Git ou através das associações.

As contribuições directas são submetidas na nossa plataforma do GitLab e revistas pela nossa equipa antes de serem incorporadas. Embora as contribuições *possam* ser feitas através do site do GitLab, recomenda-se algum conhecimento de Git porque permite aos contribuidores integrar submissões a partir dos seus sistemas, permitindo uma melhor integração com qualquer ambiente de desenvolvimento que esteja a ser utilizado.

As contribuições também podem ser submetidas a algumas das associações que fazem parte do projecto. Neste caso, a associação é responsável por apresentar a contribuição através do Git. Este processo leva tempo e a sua apresentação pode demorar algum tempo a ser revista. É aconselhável submeter tudo directamente, sempre que possível.

Plataforma
----------

Esta plataforma funciona devido a uma combinação de várias ferramentas que estão integradas entre si.

### Website

Se conhece alguma das tecnologias web utilizadas (HTML, CSS, JS, GoHugo) e gostaria de contribuir, por favor consulte este [repositório](https://gitlab.com/phoss/projects/lektures/lektures-web).

### Automation

Se gosta de automação (Bash, Docker, CI/CD) e quer contribuir, por favor consulte este [repositório](https://gitlab.com/phoss/projects/lektures/lektures-deploy).
É aqui que são necessárias menos contribuições, por isso certifique-se que apenas submete algo que beneficie muito o projecto (novas funcionalidades, automação mais robusta, etc.). Não submeta uma proposta de alteração da imagem do Docker para outra distribuição.

Content
--------

É aqui que as contribuições são mais necessárias.
Quaisquer notas relacionadas com os campos relevantes podem ser apresentadas em formato markdown ou reStructuredText.

Revisions
---------

Esta é uma parte fundamental do projecto reservada às associações envolvidas e aos indivíduos academicamente certificados.
Apenas os membros que frequentaram cursos na área relevante podem aceitar a apresentação de conteúdos dessa área.

Translations
------------

Embora o foco principal do projeto seja o português, o site pode suportar outras línguas e o faz.
Por enquanto, aceitamos traduções ou novos conteúdos em espanhol, alemão e especialmente em inglês.